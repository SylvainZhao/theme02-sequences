Dans ce thème nous présentons les *séquences* (paresseuses) de Clojure.

Ce projet propose quelques exercices sur ce thème.

Voici quelques exercices supplémentaires sur le même thème :

 - *Clojure koans*: exercices 09, 11, 12, 21, 22

 - *4Clojure*: exercices 17, 22 à 35, 39, 40, 41, 43, 44, 45, 49, 56, 60, 62, 64


